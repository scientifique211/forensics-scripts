import os
import time
import itertools 
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad



def decrypt_file(file_path, key, iv):
    print(f"Key: {key}, IV: {iv}")
    key = bytes.fromhex(key)
    iv = bytes.fromhex(iv)
    if len(key) != 32 or len(iv) != 16:
            raise ValueError("La longueur de la clé ou de l'IV est incorrecte.")
    
    cipher = AES.new(key, AES.MODE_CBC, iv)

    try:
        with open(file_path, 'rb') as file:
            ciphertext = file.read()
            decrypted_data = unpad(cipher.decrypt(ciphertext), AES.block_size)
        return decrypted_data
    except:
        return None


def find_keys(file_path, keys, ivs):
    for (key, iv) in zip(keys, ivs):
        decrypted_data = decrypt_file(file_path, key, iv)
        if (decrypted_data is not None):
            newpath = file_path[:-6]
            with open(newpath, 'wb') as output_file:
                output_file.write(decrypted_data)
            print(f"Fichier {file_path} a été déchiffrer avec la clés {key} et l'iv {iv} !")    
        else:
            print(f"Impossible de déchiffrer le fichier {file_path} !")

if __name__ == "__main__":
    encryptdir = "D:/Henallux/Block 3/ForensicsV2/lock"
    keys = ['763d35c056ef338be059e0cde3b94f1fb0cca3aea6c3a485e6ad11c02d4bc85f', 'bf8443696c4833c2eae7dbfc6f5aacf6ede1cf6815764387440918fee8c693ed', 'db8f093fa1ad7f860a7dbe8de0f81e0e41da5248eb775aa9af5e12ccba20d050',
            'f6ea73feb7f391cae303a8200e5426890e4361dc976af7e3dc38bd1dc4f6935f', '9dc9b8436641d401c3127d5eb406085b5a751484b86e8e4541164c3192267fdf', '239435e1322333a016d1b9768babce481c51800fbbc6cfa5983cdead83f06165',
            '7ff5d94afd5efdbb4eaa6a240ce6df9af543d40a6a48b5e86af9aa6ba6e59e62', '658ae514db0c34ae93ce764f3880cc658925921bb39a2f220edc2c9d4f398e7a', '3c97b412df6ea4c3f88e896a6a78c2ad58b6e113a0af4696e782e28b898a1096',
            '5f8a2f20a47ef83ad65040d9460cccf028a47c60b040d6f1eeaab44707ff723c', '8a2f1c2fbcc7dd171db2e48b4a0716db56ba336a45e2bb8aec1626bbab089e55', '0d83896eeb637f8d8beed1276f95b1b11bf7a38c7710c40c7c85dc9685c74158',
            'b96b27c6aaa3c71933308844fb76470de29723ca62e19a020f43837d01eb4731']
    ivs = ['a85a7b56bd76fd54aa1ae65bb69c91fd', 'd8a77552aad9887d5b7d53ac4ad15bdf', '92827e23c3c7571d8d99a033f6b229d3', '64d7c9763edfc50a28cc94abccadec50', '88a4e7b0c7053e58b77b043fe71e7168',
           'd39ec0a019a9d98b8c21b6111fb91f75', '3b25b440e44d126b64659796852716eb', '2379454c0b4e6edc11a24398ea8a123e', '734d00777a1dee3d9da233d09b8027e7', 'e5c31431c210e518a4f496928c8d1813',
           '159a48ea06b7582999751b4c68318e4d', 'c949061d469118e277b88058cca2e5f0', '9f2b1ccb2974aa645a22e67fc3cc5066']
    filenumber = 0
    starttime = time.time()

    for files in os.listdir(encryptdir):
        if files.endswith(".pachy"):
            filenumber+=1
            encrypt_file = os.path.join(encryptdir, files)
            find_keys(encrypt_file, keys, ivs)
            print("File read: "+str(filenumber))
            currenttime = time.time()-starttime
            print("Elapsed time: "+str(currenttime))
        else:
            print(f"Not a encrypt file: {files}")
