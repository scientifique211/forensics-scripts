import os
import socket
from scapy.all import *
import concurrent.futures
import time

def extract_hosts_from_pcap(pcap_file):
    hosts = {}
    print(f"Reading: {pcap_file}")
    packets = rdpcap(pcap_file)
    print("File is ready to iterate")

    print(f"Started processing {pcap_file}")

    completefile = open("packetkeyiv.txt", "a")

    iskey = 0
    combined = ""

    for packet in packets:
        if packet.haslayer("UDP"):
            try:
                date = str(datetime.fromtimestamp(int(packet.time)))
                packetip = packet[IP].dst
                if packetip == "51.38.185.89":
                    iskey+=1
                    packetdata = packet.load
                    datas = packetdata.split(b'\x00')[0]
                    combined = combined+datas.decode('utf-8')
                    if iskey > 3:
                        iskey = 0
                        completefile.write(combined+",\n")
                        print(combined+"\n")
                        combined = ""

            except:
                pass

    completefile.close()
    print(f"Finished processing {pcap_file}")


if __name__ == "__main__":
    pcapdir = "F:/Henallux/Block 3/ForensicsV2/logs"
    filenumber = 0
    starttime = time.time()

    for files in os.listdir(pcapdir):
        if files.endswith(".pcap"):
            filenumber+=1
            pcap_file = os.path.join(pcapdir, files)
            extract_hosts_from_pcap(pcap_file)
            print("File read: "+str(filenumber))
            currenttime = time.time()-starttime
            print("Elapsed time: "+str(currenttime))
        else:
            print(f"Not a pcap: {files}")
