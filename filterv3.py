import os
import socket
from scapy.all import *
import concurrent.futures
import time

skipdomain = {"microsoft.com","windowsupdate.com","ubuntu.com","debian.org"}

def not_exist(domain, listd):
    unique = True
    for domainIT in listd:
        domainSplit = domain.split(".")
        if domainSplit[-2] in listd[domainIT]:
            unique = False
    return unique


def toskip(domain):
    for toskip in skipdomain:
        if toskip in domain:
            return True

    return False

def extract_hosts_from_pcap(pcap_file, resolved_results):
    hosts = {}
    print(f"Reading: {pcap_file}")
    packets = rdpcap(pcap_file)
    print("File is ready to iterate")

    print(f"Started processing {pcap_file}")

    completefile = open("complelog.txt", "a")
    domainfile = open("domainlog.txt", "a")

    domainfile.write("File : "+pcap_file+"\n")
    completefile.write("File : "+pcap_file+"\n")

    for packet in packets:
        try:
            date = str(datetime.fromtimestamp(int(packet.time)))
            if packet.haslayer('HTTPRequest'):
                url = packet[HTTPRequest].Host.decode() + packet[HTTPRequest].Path.decode()
                if not toskip(url):
                    tolog = date+" >> "+packet[IP].src+" ==> "+packet[IP].dst+" ("+packet[HTTPRequest].Method.decode()+") : "+url
                    print(tolog)
                    completefile.write(tolog+"\n")
                    if not_exist(hstname, resolved_results):
                        resolved_results[packet[IP].dst] = hstname
                        domainfile.write(tolog+"\n")

            if packet.haslayer('TLS') and packet['TLS'].type == 22 and packet['TLS'].msg[0].msgtype == 1:
                hstname = (packet['TLS']['TLS_Ext_ServerName'].servernames[0].servername).decode("utf-8")
                msg = packet['IP'].src +' ==> ' +packet['IP'].dst +' : '+ hstname
                tolog = date+" >> "+msg
                if not toskip(hstname):
                    print(tolog)
                    completefile.write(tolog+"\n")
                    if not_exist(hstname, resolved_results):
                        resolved_results[packet[IP].dst] = hstname
                        domainfile.write(tolog+"\n")

        except:
            print("Error with "+str(datetime.fromtimestamp(int(packet.time)))+" >> "+packet[IP].src+" ==> "+packet[IP].dst)
            pass

    completefile.close()
    domainfile.close()
    print(f"Finished processing {pcap_file}")


if __name__ == "__main__":
    pcapdir = "D:/Henallux/Block 3/ForensicsV2/logs"
    resolved_results = {}
    load_layer("tls")
    load_layer("http")
    filenumber = 0
    starttime = time.time()

    for files in os.listdir(pcapdir):
        if files.endswith(".pcap"):
            filenumber+=1
            pcap_file = os.path.join(pcapdir, files)
            extract_hosts_from_pcap(pcap_file, resolved_results)
            print("File read: "+str(filenumber))
            currenttime = time.time()-starttime
            print("Elapsed time: "+str(currenttime))
        else:
            print(f"Not a pcap: {files}")

    for destination_ip, hostname in resolved_results.items():
        readable = hostname.split(".")
        print(f"{destination_ip} resolved to {readable[-2]}.{readable[-1]}")
